



#ifndef __MICROPOLIS_DRAWINGAREA__
#define __MICROPOLIS_DRAWINGAREA__

#include <string>

#include <gtkmm/drawingarea.h>

#include "tiledrawingarea.h"

class Micropolis;
class TileEngine;

class MicropolisDrawingArea
    : public TileDrawingArea
{
public:
    MicropolisDrawingArea(const std::string & _datadir);
    virtual ~MicropolisDrawingArea();
    virtual void createEngine();
    virtual void configTileEngine(TileEngine *tengine);
    virtual void destroyEngine();
    virtual int getTile(int row, int col);
    virtual void tickEngine();
private:
    Micropolis *m_engine;
    std::string m_resourcedir;
};

#endif
