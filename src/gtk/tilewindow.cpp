


#include "tilewindow.h"
#include "micropolisdrawingarea.h"


TileWindow::TileWindow()
{
    set_title("Gnome-o-polis");

    da = Gtk::manage(new MicropolisDrawingArea("./Micropolis/python/"));
    add(*da);
    show_all();
}
