



#ifndef __TRACE_H_
#define __TRACE_H_

#include <stdio.h>

#define TRACE printf("%s\n", __PRETTY_FUNCTION__)

#define TRACESHORT printf("%s\n", __FUNCTION__)

#endif
