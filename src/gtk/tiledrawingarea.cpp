/*
  Translated from python from tilewindow.py
*/

#include <math.h>
#include <stdlib.h>

#include <cairo.h>

#include <glibmm/main.h>

#include "trace.h"
#include "tiledrawingarea.h"
#include "TileEngine/tileengine.h"

TileDrawingArea::TileDrawingArea(const std::string & _datadir, 
                                 int tileCount, int tileSize, 
                    int worldCols, int worldRows)
    : m_datadir(_datadir)
    , m_tengine(NULL)
    , m_timerActive(false)
    , m_panX(0), m_panY(0)
    , m_scale(1.0)
    , m_cursorCol(0), m_cursorRow(0)
    , m_worldCols(worldCols /*256*/)
    , m_worldRows(worldRows /*256*/)
    , m_running(true)
    , m_renderCol(0)
    , m_renderRow(0)
    , m_renderCols(256)
    , m_renderRows(256)
    , m_tilesLoaded(false)
    , m_sourceTileSize(32)
    , m_tilesFileName(m_datadir + "/images/tiles.png")
    , m_tilesSourceWidth(0)
    , m_tilesSourceHeight(0)
    , m_tilesSourceCols(0)
    , m_tilesSourceRows(0)
    , m_tilesWidth(0)
    , m_tilesHeight(0)

    , m_tileCount(tileCount /*256*/)
    , m_maxSurfaceSize(512)
    , m_tileSize(tileSize /*1*/)
    , m_tileCacheCount(0)
    , m_down(false)
    , m_downX(0), m_downY(0)
    , m_downPanX(0), m_downPanY(0)
    , m_panning(false)
    , m_buffer(NULL)
    , m_bufferWidth(0)
    , m_bufferHeight(0)
    , m_windowBuffer(NULL)
    , m_windowBufferWidth(0)
    , m_windowBufferHeight(0)
    , m_mouseX(0), m_mouseY(0)
{
    TRACE;
    printf("datadir=%s\n", m_datadir.c_str());

//    createEngine();
//    set_flags(Gtk::CAN_FOCUS);

    set_events(
        Gdk::EXPOSURE_MASK |
        Gdk::POINTER_MOTION_MASK |
        Gdk::POINTER_MOTION_HINT_MASK |
        Gdk::BUTTON_MOTION_MASK |
        Gdk::BUTTON_PRESS_MASK |
        Gdk::BUTTON_RELEASE_MASK |
        Gdk::FOCUS_CHANGE_MASK |
        Gdk::KEY_PRESS_MASK |
        Gdk::KEY_RELEASE_MASK |
        Gdk::PROXIMITY_IN_MASK |
        Gdk::PROXIMITY_OUT_MASK);

    signal_draw().connect(sigc::mem_fun(*this,&TileDrawingArea::handleDraw));
    signal_focus_in_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleFocusIn));
    signal_focus_out_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleFocusOut));
    signal_key_press_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleKeyPress));
    signal_motion_notify_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleMotionNotify));
    signal_button_press_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleButtonPress));
    signal_button_release_event().connect(sigc::mem_fun(*this, &TileDrawingArea::handleButtonRelease));
    startTimer();
}

TileDrawingArea::~TileDrawingArea()
{
    TRACE;
    stopTimer();
    if(m_tengine) {
        delete m_tengine;
    }
}

void TileDrawingArea::startTimer()
{
    if(m_timerActive) {
        return;
    }
    m_timerId = Glib::signal_timeout().connect(
	    sigc::mem_fun(*this,
			  &TileDrawingArea::tickTimer),
                 1000);
    m_timerActive = true;
}


void TileDrawingArea::stopTimer()
{
//    TRACE;
    m_timerActive = false;
    m_timerId.disconnect();
}


bool TileDrawingArea::tickTimer()
{
    if(!m_timerActive) {
        return false;
    }
    if(m_running) {
        tickEngine();
    }
    queue_draw();
    m_timerActive = false;
    return false;
}


void TileDrawingArea::changeScale(float scale)
{
//    TRACE;
    int cursorScreenX = m_panX + (m_cursorCol * m_tileSize);
    int cursorScreenY = m_panY + (m_cursorRow * m_tileSize);
    int cursorCenterScreenX = cursorScreenX + (0.5 * m_tileSize);
    int cursorCenterScreenY = cursorScreenY + (0.5 * m_tileSize);

    setScale(scale);
    
    cursorScreenX = cursorCenterScreenX - (0.5 * m_tileSize);
    cursorScreenY = cursorCenterScreenY - (0.5 * m_tileSize);

    m_panX = cursorScreenX - (m_cursorCol * m_tileSize);
    m_panY = cursorScreenY - (m_cursorRow * m_tileSize);
}

void TileDrawingArea::setScale(float scale)
{
//    TRACE;
    if(m_scale == scale) {
        return;
    }
    Cairo::RefPtr<Cairo::Context> ctxWindow(get_window()->create_cairo_context());
    loadGraphics(ctxWindow, true);
    queue_draw();
}


void TileDrawingArea::loadGraphics(const Cairo::RefPtr<Cairo::Context> &ctx, bool force)
{
//    TRACE;
    if(force || (!m_tilesLoaded)) {
        loadTiles(ctx);
        m_tilesLoaded = true;
    }
}


void TileDrawingArea::loadTiles(const Cairo::RefPtr<Cairo::Context> &ctx)
{
//    TRACE;
    m_tilesSourceSurface 
        = Cairo::ImageSurface::create_from_png(m_tilesFileName);
    
    m_tilesSourceWidth = m_tilesSourceSurface->get_width();
    m_tilesSourceHeight = m_tilesSourceSurface->get_height();
    m_tilesSourceCols = floor(m_tilesSourceWidth / m_sourceTileSize);
    m_tilesSourceRows = floor(m_tilesSourceHeight / m_sourceTileSize);
    
    Cairo::RefPtr<Cairo::Surface> nativeTarget(ctx->get_target());

    if (m_scale == 1.0) {
// We don't need any scaling, so copy directly to a surface of the same size.
// All tiles are pre-cached.
        int surfaceIndex = 0;
        m_tileCache.clear();
        for(int tile = 0; tile < m_tileCount; tile++) {
            int col = tile % m_tilesSourceCols;
            int row = ::floor(tile / m_tilesSourceCols);
            m_tileCache.push_back(1);
            m_tileCache.push_back(surfaceIndex);
            m_tileCache.push_back(col * m_sourceTileSize);
            m_tileCache.push_back(row * m_sourceTileSize);
        }
        printf("ALL TILES ARE PRE-CACHED tileCount=%d tileCacheSize=%lu\n", m_tileCount, m_tileCache.size());
            
        m_tileSize = m_sourceTileSize;
        m_tilesWidth = m_tilesSourceWidth;
        m_tilesHeight = m_tilesSourceHeight;
        
        printf("MAKING TILESSURFACE tilesW=%d tileH%d\n", m_tilesWidth, m_tilesHeight);
        Cairo::RefPtr<Cairo::Surface> tilesSurface 
            = Cairo::Surface::create(nativeTarget, Cairo::CONTENT_COLOR, 
                                     m_tilesWidth, m_tilesHeight);
        printf("DONE\n");
        
        Cairo::RefPtr<Cairo::Context> tilesCtx = Cairo::Context::create(tilesSurface);
        tilesCtx->set_source(m_tilesSourceSurface, 0, 0);
        tilesCtx->paint();

        m_tileCacheSurfaces.clear();
        m_tileCacheSurfaces.push_back(tilesSurface);
        m_tileCacheCount = m_tileCount;

        m_tileSurface.clear();
        m_tileCtx.clear();
    }
    else {
// No tiles are pre-cached.
        m_tileCache.clear();// = array.array('i', (0, 0, 0, 0,) * (self.tileCount));
        m_tileCache.resize(m_tileCount * 4);
        
        printf("NO TILES ARE PRE-CACHED tileCount=%d tileChache.size=%lu\n", 
               m_tileCount, m_tileCache.size());
            
        m_tileSize = std::max(1.0, floor(0.5 + (m_scale * m_sourceTileSize)));
        
        m_tilesWidth = m_tileSize * m_tilesSourceCols;
        m_tilesHeight = m_tileSize * m_tilesSourceRows;
        
        printf("tileSize=%d tilesSourceCols=%d tilesSourceRows=%d "
               "tilesWidth=%d tilesHeight=%d\n", m_tileSize, 
               m_tilesSourceCols, m_tilesSourceRows, 
               m_tilesWidth, m_tilesHeight);

//            # Clip each tile through this client side 888 surface.
        m_tileSurface = Cairo::Surface::create(m_tilesSourceSurface, 
                                               Cairo::CONTENT_COLOR, 
                                               m_sourceTileSize, 
                                               m_sourceTileSize);
        m_tileCtx = Cairo::Context::create(m_tileSurface);
            
        m_tileCacheSurfaces.clear();
        m_tileCacheCount = 0;
    }

    makeTileMap();
    revealCursor();
}


void TileDrawingArea::makeTileMap()
{
    TRACE;
    m_tileMap.clear();
    for(int i = 0 ; i < m_tileCount; i++) {
        m_tileMap.push_back(i);
    }
}

int TileDrawingArea::getTile(int col, int row)
{
    TRACE;
    int r = (random() / RAND_MAX) * m_tileCount; 
    printf("random = %d\n", r);
    return r;
}

void TileDrawingArea::createEngine()
{
    TRACE;
    if(!m_tengine) {
        m_tengine = new TileEngine();
        configTileEngine(m_tengine);
    }
}

void TileDrawingArea::configTileEngine(TileEngine *tengine)
{
    TRACE;
    tengine->setBuffer(NULL);
    tengine->width = 1;
    tengine->height = 1;
    tengine->colBytes = 0;
    tengine->rowBytes = 0;
    tengine->typeCode = 'H';
    tengine->tileMask = 0xffffffff;
}

void TileDrawingArea::destroyEngine()
{
    TRACE;
    delete m_tengine;
    m_tengine = NULL;
}

bool TileDrawingArea::handleDraw(const ::Cairo::RefPtr< ::Cairo::Context>& cr)
{
//    TRACE;
    draw(cr);
    return false;
}


void TileDrawingArea::draw(const Cairo::RefPtr<Cairo::Context>& ctxWindow)
{
    TRACE;

    loadGraphics(ctxWindow);

    Gdk::Rectangle winRect = get_allocation();
    int winWidth = winRect.get_width();
    int winHeight = winRect.get_height();

    printf("WINRECT=(%d,%d,%d,%d) winX=%d winY=%d winWidth=%d winHeight=%d\n", 
           winRect.get_x(),winRect.get_y(),
           winRect.get_width(),winRect.get_height(), 
           0, 0, winWidth, winHeight);

    int slop = 4;

    if ((!m_windowBuffer) ||
        (m_windowBufferWidth < winWidth) ||
        (m_windowBufferHeight < winHeight))
    {
        Cairo::RefPtr<Cairo::Surface> nativeTarget = ctxWindow->get_target();
        m_windowBufferWidth = std::max(m_windowBufferWidth, winWidth + slop);
        m_windowBufferHeight = std::max(m_windowBufferHeight, winHeight + slop);

//print "Window Buffer growing from", self.m_windowBufferWidth, self.m_windowBufferHeight, "to", m_windowBufferWidth, m_windowBufferHeight

        m_windowBuffer = Cairo::Surface::create(nativeTarget, 
                                                Cairo::CONTENT_COLOR, 
                                                m_windowBufferWidth, 
                                                m_windowBufferHeight);
    }
    Cairo::RefPtr<Cairo::Context> ctxWindowBuffer = Cairo::Context::create(m_windowBuffer);
    
    int worldWidth = m_worldCols * m_tileSize;
    int worldHeight = m_worldRows * m_tileSize;
    int worldRight = m_panX + worldWidth;
    int worldBottom = m_panY + worldHeight;

    printf("WORLDRIGHT=%d WORLDBOTTOM=%d\n", worldRight, worldBottom);

//    int worldLeftClipped = std::max(winX, winX + m_panX);
    int worldTopClipped = std::max(0, m_panY);
//    int worldRightClipped = std::min(worldWidth + winX + m_panX, winX + winWidth);
    int worldBottomClipped = std::min(worldHeight + m_panY, winHeight);

    int colLeft = std::max(0.0, floor((0 - m_panX) / m_tileSize));
    
    int colRight = std::min((double)m_worldCols, ceil((winWidth - m_panX) / m_tileSize));

    printf("COLLEFT=%d COLRIGHT=%d winWidth-m_panX=%d\n", colLeft, colRight, winWidth - m_panX);
    
    int rowTop = std::max(0.0f, floor(float(0 - m_panY) / float(m_tileSize)));
    
    int rowBottom = std::min((double)m_worldRows, ceil((winHeight - m_panY) /
                                               m_tileSize));

    printf("ROWTOP=%d ROWBOTTOM=%d winHeight - m_panY=%d\n", rowTop, 
           rowBottom, winHeight - m_panY);
    
    m_renderCols = colRight - colLeft;
    m_renderRows = rowBottom - rowTop;

    printf("PANX=%d PANY=%d TILESIZE=%d COLS=%d  ROWS=%d\n", m_panX, m_panY, 
           m_tileSize, m_renderCols, m_renderRows);

    int renderX = m_panX + (colLeft * m_tileSize);
    int renderY = m_panY + (rowTop * m_tileSize);
    int renderWidth = m_renderCols * m_tileSize;
    int renderHeight = m_renderRows * m_tileSize;

    m_renderCol = colLeft;
    m_renderRow = rowTop;

//Make the off-screen buffer to draw the tiles into.

    if ((!m_buffer) || (m_bufferWidth < renderWidth) ||
        (m_bufferHeight < renderHeight))
    {
        Cairo::RefPtr<Cairo::Surface> nativeTarget = ctxWindow->get_target();
        m_bufferWidth = std::max(m_bufferWidth, renderWidth + slop);
        m_bufferHeight = std::max(m_bufferHeight, renderHeight + slop);

        printf("M_Buffer growing from %d,%d to %d,%d\n", m_bufferWidth, 
               m_bufferHeight, m_bufferWidth, m_bufferHeight);

        m_buffer = Cairo::Surface::create(nativeTarget,
                                          Cairo::CONTENT_COLOR, 
                                          m_bufferWidth, m_bufferHeight);
    }
    {
        Cairo::RefPtr<Cairo::Context> ctx = Cairo::Context::create(m_buffer);
        
        if ((m_renderCols > 0) && (m_renderRows > 0)) {
            m_tengine->renderTilesLazy(
                ctx,
                m_tileMap,
                m_tileSize,
                m_renderCol,
                m_renderRow,
                m_renderCols,
                m_renderRows,
                1.0,
                sigc::mem_fun(this,&TileDrawingArea::generateTile),
                m_tileCache,
                m_tileCacheSurfaces);
        }
    }
    ctxWindowBuffer->save();
    ctxWindowBuffer->set_source(m_buffer, renderX,
                                renderY);
    ctxWindowBuffer->rectangle(renderX,
                               renderY,
                               renderWidth,
                               renderHeight);
    ctxWindowBuffer->clip();
    ctxWindowBuffer->paint();

    ctxWindowBuffer->restore();
    
//Draw the background outside of the tile world.
    
    bool backgroundVisible = false;
        
//Left Background
    if (m_panX > 0) {
        backgroundVisible = true;
        ctxWindowBuffer->rectangle(0,
                                   worldTopClipped,
                                   m_panX,
                                   worldBottomClipped - worldTopClipped);
    }
// Right Background
    if (worldRight < winWidth) {
        backgroundVisible = true;
        ctxWindowBuffer->rectangle(0 + worldRight,
                                   worldTopClipped,
                                   winWidth - worldRight,
                                   worldBottomClipped - worldTopClipped);
    }
// Top Background
    if (m_panY > 0) {
        backgroundVisible = true;
        ctxWindowBuffer->rectangle(0,
                                   0,
                                   winWidth,
                                   m_panY);
    }
// Bottom Background
    if (worldBottom < winHeight) {
        backgroundVisible = true;
        ctxWindowBuffer->rectangle(0,
                                   worldBottom + 0,
                                   winWidth,
                                   winHeight - worldBottom);
    }
    if (backgroundVisible) {
        ctxWindowBuffer->set_source_rgb(0.5, 0.5, 0.5);
        ctxWindowBuffer->fill();
    }
    drawOverlays(ctxWindowBuffer);
    
    ctxWindow->set_source(m_windowBuffer, 0, 0);
    ctxWindow->paint();
    
    if (m_running) {
        startTimer();
    }
}

std::tuple<int, int, int> TileDrawingArea::generateTile(int tile)
{
    TRACE;
// This function is called from the C++ code in self.tengine.renderTilesLazy.
// It renders a tile, and returns a tuple with a surface index, tile x and tile y position. 
// This function is totally in charge of the scaled tile cache, and can implement a variety 
// variety of different policies. 

    int tileColsPerSurface = std::max(1.0, floor(m_maxSurfaceSize / m_tileSize));
//#print "tileColsPerSurface", tileColsPerSurface
        
    int tilesPerSurface = tileColsPerSurface * tileColsPerSurface;
//#print "tilesPerSurface", tilesPerSurface
        
    int surfaceSize = tileColsPerSurface * m_tileSize;
//#print "surfaceSize", surfaceSize
        
    int cacheTile = m_tileCacheCount;
    m_tileCacheCount += 1;
       
    unsigned int surfaceIndex = floor(cacheTile / tilesPerSurface);
//#print "surfaceIndex", surfaceIndex
        
    Cairo::RefPtr<Cairo::Context> ctxWindow;
    Cairo::RefPtr<Cairo::Surface> nativeTarget;
    Cairo::RefPtr<Cairo::Surface> tilesSurface;
    while (m_tileCacheSurfaces.size() <= surfaceIndex) {
//            #print "MAKING TILESSURFACE", len(tileCacheSurfaces), tilesPerSurface, surfaceSize
        if (!nativeTarget) {
            ctxWindow = get_window()->create_cairo_context();
            nativeTarget = ctxWindow->get_target();
        }
        tilesSurface = Cairo::Surface::create(nativeTarget,
                                              Cairo::CONTENT_COLOR, 
                                              surfaceSize, surfaceSize);
        m_tileCacheSurfaces.push_back(tilesSurface);
//            #print "DONE"
    }
    tilesSurface = m_tileCacheSurfaces[surfaceIndex];
    int tileOnSurface = cacheTile % tilesPerSurface;
//        print "tileOnSurface", tileOnSurface
    int tileCol = tileOnSurface % tileColsPerSurface;
    int tileRow = floor(tileOnSurface / tileColsPerSurface);
//        print "tileCol", tileCol, "tileRow", tileRow
    int tileX = tileCol * m_tileSize;
    int tileY = tileRow * m_tileSize;
//       print "tileX", tileX, "tileY", tileY
    int sourceTileCol = tile % m_tilesSourceCols;
    int sourceTileRow = floor(tile / m_tilesSourceCols);
//    print "sourceTileCol", sourceTileCol, "sourceTileRow", sourceTileRow

    // Make a temporary tile size of a source tile.
    m_tileCtx->set_source(m_tilesSourceSurface,
                                  -sourceTileCol * m_sourceTileSize,
                                  -sourceTileRow * m_sourceTileSize);
    m_tileCtx->paint();

    Cairo::RefPtr<Cairo::Context> tilesCtx = Cairo::Context::create(tilesSurface);
    tilesCtx->set_source(m_tilesSourceSurface, 0, 0);
    
    // Scale it down into the tilesSurface.
    tilesCtx->save();
    
    int x = tileCol * m_tileSize;
    int y = tileRow * m_tileSize;

    tilesCtx->rectangle(x, y, m_tileSize, m_tileSize);
    tilesCtx->clip();

//        Try to keep the tiles centered.
    int fudge = 0; // (0.5 * (scale - tileSize))
    
    x += fudge;
    y += fudge;
    
    tilesCtx->translate(x, y);
//        print "X", x, "Y", y, "FUDGE", fudge, "SCALE", scale, "TILESIZE", tileSize
    float zoomScale = m_tileSize / m_sourceTileSize;
//        #print "ZOOMSCALE", zoomScale, "TILESIZE", tileSize, "SOURCETILESIZE", sourceTileSize
    
    tilesCtx->scale(zoomScale, zoomScale);
    
    const float deltas[18] = {-0.5, -0.5,
                              0.5, -0.5,
                              -0.5,  0.5,
                              0.5,  0.5,
                              -0.5,  0.0,
                              0.5,  0.0,
                              0.0, -0.5,
                              0.0,  0.5,
                              0.0,  0.0 };

    for (int i = 0; i < 18; i += 2) {
        tilesCtx->save();
        tilesCtx->translate(deltas[i], deltas[i+1]);
        tilesCtx->set_source(m_tileSurface, 0, 0);
        tilesCtx->paint();
        tilesCtx->restore();
    }
    tilesCtx->restore();
    
    //      print "GENERATETILE", tile, "surfaceIndex", surfaceIndex, "tileX", tileX, "tileY", tileY
    
    //      print "GENERATETILE", tile, "RESULT", result
    return std::tuple<int,int,int>(surfaceIndex, tileX, tileY);
}

void TileDrawingArea::drawOverlays(const Cairo::RefPtr<Cairo::Context> &ctx)
{
    drawCursor(ctx);
}


void TileDrawingArea::drawCursor(const Cairo::RefPtr<Cairo::Context> &ctx)
{
    int x, y;
    x = m_panX + (m_tileSize * m_cursorCol);
    y = m_panY + (m_tileSize * m_cursorRow);
    ctx->save();
    ctx->translate(x, y);
    ctx->rectangle(-2, -2, m_tileSize + 4, m_tileSize + 4);
    ctx->set_line_width(4.0);
    ctx->set_source_rgb(1.0, 1.0, 1.0);
    ctx->stroke_preserve();
    ctx->set_line_width(2.0);
    ctx->set_source_rgb(0, 0, 0);
    ctx->stroke();
    ctx->restore();
    int tileCode = getTile(m_cursorCol, m_cursorRow);
    std::string tileName = std::to_string(tileCode);
    std::string tileDescription = "Tile #" + tileName;
}


void TileDrawingArea::setCursorPos(int col, int row)
{
    col = std::max(0, std::min(m_worldCols - 1, col));
    row = std::max(0, std::min(m_worldRows - 1, row));
    if((m_cursorCol != col) || (m_cursorRow != row)) {
        m_cursorCol = col;
        m_cursorRow = row;
        queue_draw();
    }
}

void TileDrawingArea::moveCursorToMouse(int x, int y)
{
    int col = floor((x - m_panX) / m_tileSize);
    int row = floor((y - m_panY) / m_tileSize);
    setCursorPos(col, row);
}

void TileDrawingArea::moveCursor(int dx, int dy)
{
    int col = m_cursorCol + dx;
    int row = m_cursorRow + dy;

    if (col < 0)
        col = 0;
    if (col >= m_worldCols)
        col =  m_worldCols - 1;

    if (row < 0)
        row = 0;
    if (row >= m_worldRows)
        row = m_worldRows - 1;
    
    setCursorPos(col, row);
    revealCursor();
}

void TileDrawingArea::revealCursor()
{
    Gdk::Rectangle rect = get_allocation();
    int winX = rect.get_x();
    int winY = rect.get_y();
    int winWidth = rect.get_width();
    int winHeight = rect.get_height();
    
    int left = m_panX + (m_tileSize * m_cursorCol);
    int right = left + m_tileSize;
    int top = m_panY + (m_tileSize * m_cursorRow);
    int bottom = top + m_tileSize;
    
    int dx = 0;
    int dy = 0;

    if (right >= (winX + winWidth)) {
        dx = (winX + winWidth) - right;
    }
    else if (left < winX) {
        dx = winX - left;
    }

    if (bottom >= (winY + winHeight)) {
        dy = (winY + winHeight) - bottom;
    }
    else if (top < winY) {
        dy = winY - top;
    }

    if (dx || dy) {
        //print "Panning", dx, dy
        m_panX += dx;
        m_panY += dy;
        queue_draw();
    }
}

bool TileDrawingArea::handleFocusIn(GdkEventFocus *event)
{
    return false;
}


bool TileDrawingArea::handleFocusOut(GdkEventFocus *event)
{
    return false;
}


bool TileDrawingArea::handleKeyPress(GdkEventKey *event)
{
    guint key = event->keyval;
    
    if (key == 'i') {
        changeScale(m_scale * 1.1);
    }
    else if (key == 'I') {
        changeScale(m_scale * 2.0);
    }
    else if (key == 'o') {
        changeScale(m_scale * 0.9);
    }
    else if (key == 'O') {
        changeScale(m_scale * 0.5);
    }
    else if (key == 'r') {
        changeScale(1.0);
    }
    else if (key == 65362) {
        moveCursor(0, -1);
    }
    else if (key == 65364) {
        moveCursor(0, 1);
    }
    else if (key == 65361) {
        moveCursor(-1, 0);
    }
    else if (key == 65363) {
        moveCursor(1, 0);
    }
    else {
//            print "KEY", event.keyval
    }
    return false;
}

bool TileDrawingArea::handleMotionNotify(GdkEventMotion *event)
{
    return updateCursorPosition(event);
}

bool TileDrawingArea::updateCursorPosition(GdkEventMotion *event)
{
    int x, y;
    GdkModifierType state;
    if (event->is_hint) {
        gdk_window_get_device_position(event->window, event->device,
				       &x, &y, &state);
    }
    else {
        x = event->x;
        y = event->y;
        state = (GdkModifierType)event->state;
    }

    m_mouseX = x;
    m_mouseY = y;

    moveCursorToMouse(x, y);

    if (m_down) {
        handleDrag();
    }
    return false;
}

void TileDrawingArea::handleDrag()
{
//print "handleDrag TileDrawingArea", self

    int x = m_mouseX;
    int y = m_mouseY;
    
    if (m_panning) {
//print "PANNING"
        int dx = x - m_downX;
        int dy = y - m_downY;
        int panX = m_downPanX + dx;
        int panY = m_downPanY + dy;
        if ((panX != m_panX) || (panY != m_panY)) {
            m_panX = panX;
            m_panY = panY;
            queue_draw();
        }
    }
}

bool TileDrawingArea::handleButtonPress(GdkEventButton *event)
{
//print "handleButtonPress TileDrawingArea", self
    
    m_down = true;
    m_downX = event->x;
    m_downY = event->y;

    m_panning = true;
    m_downPanX = m_panX;
    m_downPanY = m_panY;

    handleDrag();
    return false;
}

bool TileDrawingArea::handleButtonRelease(GdkEventButton *event)
{
//print "handleButtonRelease TileDrawingArea", self
    handleDrag();
    m_down = false;
    m_panning = false;
    return false;
}


