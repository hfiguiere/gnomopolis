/*
  Translated from Python from micropoliswindow.py
 */

#include "Micropolis/micropolis.h"
#include "TileEngine/tileengine.h"
#include "micropolisdrawingarea.h"
#include "trace.h"

MicropolisDrawingArea::MicropolisDrawingArea(const std::string & _datadir)
    : TileDrawingArea(_datadir, 960, 16, WORLD_X, WORLD_Y)
    , m_engine(NULL)
{
    TRACE;
    createEngine();
/*
        args['tileCount'] = 960
        args['sourceTileSize'] = 16
        args['worldCols'] = micropolis.WORLD_X
        args['worldRows'] = micropolis.WORLD_Y
*/
}

MicropolisDrawingArea::~MicropolisDrawingArea()
{
    if(m_engine) {
        delete m_engine;
    }
}

void MicropolisDrawingArea::createEngine()
{
    TRACE;
    m_engine = new Micropolis();    
    
    m_resourcedir = m_datadir + "res";
    m_engine->ResourceDir = m_resourcedir.c_str();
    m_engine->InitGame();
        
// Load a city file.
    const char* cityFileName = std::string(m_datadir + "/cities/haight.cty").c_str();
//        print "Loading city file:", cityFileName
    m_engine->loadFile(cityFileName);

// Initialize the simulator engine.

    m_engine->Resume();
    m_engine->setSpeed(2);
    m_engine->setSkips(100);
    m_engine->SetFunds(1000000000);
    m_engine->autoGo = 0;
    m_engine->CityTax = 9;

    TileDrawingArea::createEngine();
}

void MicropolisDrawingArea::configTileEngine(TileEngine *tengine)
{
    TRACE;
    tengine->setBuffer(m_engine->getMapBuffer());
    tengine->width = WORLD_X;
    tengine->height = WORLD_Y;
    tengine->colBytes = 2 * WORLD_Y;
    tengine->rowBytes = 2;
    tengine->typeCode = 'H';
    tengine->tileMask = LOMASK;
}

void MicropolisDrawingArea::destroyEngine()
{
    TRACE;
    delete m_engine;
    m_engine = NULL;
    TileDrawingArea::destroyEngine();
}

int MicropolisDrawingArea::getTile(int col, int row)
{
    TRACE;
    return m_engine->getTile(col,row);
}

void MicropolisDrawingArea::tickEngine()
{
//    TRACE;
    m_engine->sim_tick();
    m_engine->animateTiles();
}
