

#include <string>
#include <tuple>

#include <cairomm/surface.h>
#include <gtkmm/drawingarea.h>

class TileEngine;

class TileDrawingArea
    : public Gtk::DrawingArea
{
public:
    TileDrawingArea(const std::string & _datadir,
                    int tileCount, int tileSize, 
                    int worldCols, int worldRows);
    virtual ~TileDrawingArea();

    void startTimer();
    void stopTimer();
    bool tickTimer();
    virtual void tickEngine() = 0;
    void changeScale(float scale);
    void setScale(float scale);
    void loadGraphics(const Cairo::RefPtr<Cairo::Context> & ctx, bool force = false);
    void loadTiles(const Cairo::RefPtr<Cairo::Context> & ctx);
    void makeTileMap();
    virtual int getTile(int col, int row);
    virtual void createEngine();
    virtual void configTileEngine(TileEngine *tengine);
    virtual void destroyEngine();
    bool handleDraw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
    void draw(const ::Cairo::RefPtr< ::Cairo::Context>& cr);
    std::tuple<int, int, int> generateTile(int tile);
    void drawOverlays(const Cairo::RefPtr<Cairo::Context> &ctx);
    void drawCursor(const Cairo::RefPtr<Cairo::Context> &ctx);
    void setCursorPos(int col, int row);
    void moveCursorToMouse(int x, int y);
    void moveCursor(int dx, int dy);
    void revealCursor();
    bool handleFocusIn(GdkEventFocus *event);
    bool handleFocusOut(GdkEventFocus *event);
    bool handleKeyPress(GdkEventKey *event);
    bool handleMotionNotify(GdkEventMotion *event);
    bool updateCursorPosition(GdkEventMotion *event);
    void handleDrag();
    bool handleButtonPress(GdkEventButton *event);
    bool handleButtonRelease(GdkEventButton *event);

protected:
    std::string m_datadir;    
private:
    TileEngine *m_tengine;
    bool m_timerActive;
    sigc::connection m_timerId;

    int m_panX;
    int m_panY;
    float m_scale;
    int m_cursorCol;
    int m_cursorRow;
    int m_worldCols;
    int m_worldRows;
    bool m_running;
    int m_renderCol;
    int m_renderRow;
    int m_renderCols;
    int m_renderRows;
    bool m_tilesLoaded;
    int m_sourceTileSize;
    std::string m_tilesFileName;
    Cairo::RefPtr<Cairo::ImageSurface> m_tilesSourceSurface;
    int m_tilesSourceWidth;
    int m_tilesSourceHeight;
    int m_tilesSourceCols;
    int m_tilesSourceRows;
    Cairo::RefPtr<Cairo::Surface> m_tileSurface;
    Cairo::RefPtr<Cairo::Context> m_tileCtx;
    int m_tilesWidth;
    int m_tilesHeight;

    int m_tileCount;
    int m_maxSurfaceSize;
    int m_tileSize;
    std::vector<int> m_tileMap;
    std::vector<int> m_tileCache;
    std::vector<Cairo::RefPtr<Cairo::Surface> > m_tileCacheSurfaces;
    int m_tileCacheCount;
    bool m_down;
    int m_downX;
    int m_downY;
    int m_downPanX;
    int m_downPanY;
    bool m_panning;

    Cairo::RefPtr<Cairo::Surface> m_buffer;
    int m_bufferWidth;
    int m_bufferHeight;
    Cairo::RefPtr<Cairo::Surface> m_windowBuffer;
    int m_windowBufferWidth;
    int m_windowBufferHeight;
    int m_mouseX;
    int m_mouseY;
/*
        maxSurfaceSize=512,
        renderCol=0,
        renderRow=0,
        renderCols=256,
        renderRows=256,
        timeDelay=50,
        outsideBackgroundColor=(0.5, 0.5, 0.5),
        insideBackgroundColor=(0.0, 0.0, 0.0),
*/
};

