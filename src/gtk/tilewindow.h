


#ifndef __TILEWINDOW_H__
#define __TILEWINDOW_H__

#include <gtkmm/window.h>
#include <gtkmm/drawingarea.h>


class TileWindow
    : public Gtk::Window
{
public:
    TileWindow();

private:
    Gtk::DrawingArea *da;
};


#endif
